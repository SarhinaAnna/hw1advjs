class Employee {
  constructor(name, age, salary) {
    this._name = name;

    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }

  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

const employee = new Employee(`koly`, `20`, `200`);

console.log(employee.name);
console.log(employee.age);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get lang() {
    return this._lang;
  }
  set lang(newLang) {
    this._lang = newLang;
  }
  get salary() {
    return this._salary * 3;
  }
}

const progr = new Programmer(`a`, `12`, `200`, `english`);
const progr1 = new Programmer(`koly`, `14`, `300`, `latin`);
const progr2 = new Programmer(`lena`, `16`, `400`, `spanich`);
console.log(progr);
console.log(progr1);
console.log(progr2);
